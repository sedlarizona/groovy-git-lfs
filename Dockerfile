FROM groovy:2.5.2-jdk8
LABEL maintainer "tyler@sedlar.me"

USER root

RUN apt-get update -y \
    && apt-get install git-core -y

RUN apt-get update && apt-get -y install wget git -y && \
wget https://packagecloud.io/github/git-lfs/packages/ubuntu/xenial/git-lfs_2.1.1_amd64.deb/download -O git-lfs_2.1.1_amd64.deb && \
dpkg -i git-lfs_2.1.1_amd64.deb

USER groovy

RUN git config --global user.email "Dummy@User" \
    && git config --global user.name "Dummy User"